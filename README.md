# Docker home

This docker set up the following : Zigbee2MQTT, Mosquitto server and home-assistant 
This allows to pair Zigbee devices and add them to home-assistant through the Mosquitto server

This is supposed to work behind a proxy (here traefik), but you can remove labels if you don't use it
You have to replace the domain.tld by yours in .env file

#### Create and run home docker as detached
    docker-compose up -d

#### Create and run home docker
    docker-compose up

#### Stop home docker and related network
    docker-compose down

#### Access to logs and follow
    docker-compose logs -f

#### Pull image
    docker-compose pull

#### Open terminal on docker container
    docker containerid exec -it dockname /bin/bash

Replace all *_EXAMPLE with your own usernames/passwords

The following required modules can be installed through HACS:
- zigbee2mqtt_networkmap
- Alexa media player
- Spotcast
- Emby_upcoming_media
- Sonarr_upcoming_media

I also suggest you to install the following integration through HACS:
- Mini Graph Card
- Bar Card
- Spotify Lovelace Card (Using Spotcast integration and Spotify integration)
- Upcoming Media Card (Using Emby_upcoming_media and Sonarr_upcoming_media integration)
- Lovelace Swipe Navigation
